﻿using PDFPrintGenerator.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PrintGenerator gen = new PrintGenerator();

            var appDomain = System.AppDomain.CurrentDomain;
            var basePath = appDomain.RelativeSearchPath ?? appDomain.BaseDirectory;

            //var SectionsXmlPath = basePath + @"\HtmlTemplates\Sections.xml";

            var HeaderXmlPath = basePath + @"\HtmlTemplates\Header.xml";
            string xmlHeaderTemplate = System.IO.File.ReadAllText(HeaderXmlPath);

            var FooterXmlPath = basePath + @"\HtmlTemplates\Footer.xml";
            string xmlFooterTemplate = System.IO.File.ReadAllText(FooterXmlPath);

            var AddressSectionXmlPath = basePath + @"\HtmlTemplates\AddressSection.xml";
            string xmAddressSectionTemplate = System.IO.File.ReadAllText(AddressSectionXmlPath);
            //string xmlSectionsTemplate = System.IO.File.ReadAllText(SectionsXmlPath);


            var ConsumptionSectionXmlPath = basePath + @"\HtmlTemplates\ConsumptionSection.xml";
            string xmlConsumptionSectionTemplate = System.IO.File.ReadAllText(ConsumptionSectionXmlPath);

            var ContractualConditionsSectionXmlPath = basePath + @"\HtmlTemplates\ContractualConditionsSection.xml";
            string xmlContractualConditionsSectionTemplate = System.IO.File.ReadAllText(ContractualConditionsSectionXmlPath);

            var ExpiredDocumentSectionXmlPath = basePath + @"\HtmlTemplates\ExpiredDocumentSection.xml";
            string xmlExpiredDocumentSectionTemplate = System.IO.File.ReadAllText(ExpiredDocumentSectionXmlPath);

            var HtmlSectionXmlPath = basePath + @"\HtmlTemplates\HtmlSection.xml";
            string xmlHtmlSectionTemplate = System.IO.File.ReadAllText(HtmlSectionXmlPath);

            var InvoiceSectionXmlPath = basePath + @"\HtmlTemplates\InvoiceSection.xml";
            string xmlInvoiceSectionTemplate = System.IO.File.ReadAllText(InvoiceSectionXmlPath);

            var InvoiceSummaryXmlPath = basePath + @"\HtmlTemplates\InvoiceSummary3BoxSection.xml";
            string xmlInvoiceSummaryTemplate = System.IO.File.ReadAllText(InvoiceSummaryXmlPath);

            var ReferenceSectionXmlPath = basePath + @"\HtmlTemplates\ReferenceSection.xml";
            string ReferenceSectionTemplate = System.IO.File.ReadAllText(ReferenceSectionXmlPath);

            var SupplyDetailSectionXmlPath = basePath + @"\HtmlTemplates\SupplyDetailSection.xml";
            string xmlSupplyDetailSectionTemplate = System.IO.File.ReadAllText(SupplyDetailSectionXmlPath);

            var ChartDataSectionsXmlPath = basePath + @"\HtmlTemplates\ChartDataSections.xml";
            string xmlChartDataSectionsTemplate = System.IO.File.ReadAllText(ChartDataSectionsXmlPath);

            var DocumentXmlPath = basePath + @"\HtmlTemplates\DocumentSections.xml";
            string xmlDocumentTemplate = System.IO.File.ReadAllText(DocumentXmlPath);

            var SummaryDataSectionsXmlPath = basePath + @"\HtmlTemplates\SummaryDataSections.xml";
            string xmlSummaryDataSectionsTemplate = System.IO.File.ReadAllText(SummaryDataSectionsXmlPath);

            var EmptySectionsXmlPath = basePath + @"\HtmlTemplates\EmptySections.xml";
            string xmlEmptySectionsTemplate = System.IO.File.ReadAllText(EmptySectionsXmlPath);


            var PrescrizioneXmlPath = basePath + @"\HtmlTemplates\Prescrizione.xml";
            string xmlPrescrizioneTemplate = System.IO.File.ReadAllText(PrescrizioneXmlPath);

            var PostalFeedsXmlPath = basePath + @"\HtmlTemplates\Postal.xml";
            string xmlPostalFeedsTemplate = System.IO.File.ReadAllText(PostalFeedsXmlPath);

            var PostalFeedsXmlPathR = basePath + @"\HtmlTemplates\Test.xml";
            string xmlPostalFeedsTemplateR = System.IO.File.ReadAllText(PostalFeedsXmlPathR);

            //string Xml = "<root>" + xmlHeaderTemplate +xmlFooterTemplate+ xmAddressSectionTemplate + xmlConsumptionSectionTemplate + xmlEmptySectionsTemplate + xmlSummaryDataSectionsTemplate + xmlDocumentTemplate + xmlChartDataSectionsTemplate + xmlSupplyDetailSectionTemplate + ReferenceSectionTemplate + xmlInvoiceSummaryTemplate + xmlInvoiceSectionTemplate + xmlHtmlSectionTemplate + xmlExpiredDocumentSectionTemplate + xmlContractualConditionsSectionTemplate + "</root>";
            //  gen.ColorScheme = "blue";
            gen.HeaderFontSize = 20;
            gen.FontFamily = "calibri";
            gen.HeaderHeight = 120;
            gen.HeaderImageUrl = basePath + @"/HtmlTemplates/DefaultLogo.png";
            // gen.GeneratePrint();
            //gen.HeaderImageUrl = "https://upload.wikimedia.org/wikipedia/commons/3/33/Vanamo_Logo.png";
            //var stream = gen.GeneratePrint(Xml);
            //var stream = gen.GeneratePrint(xmlHeaderTemplate, xmlSectionsTemplate, xmAddressSectionTemplate);
            //var stream = gen.GeneratePrint(xmlHeaderTemplate, xmlSectionsTemplate);
            var PostalSheetPath = basePath + @"\HtmlTemplates\dummy.pdf";
          

            byte[] byteHeaderArray = Encoding.ASCII.GetBytes(xmlHeaderTemplate);
            MemoryStream Headerstream = new MemoryStream(byteHeaderArray);

            byte[] byteFooterArray = Encoding.ASCII.GetBytes(xmlFooterTemplate);
            MemoryStream Footerstream = new MemoryStream(byteFooterArray);

            byte[] byteAddressArray = Encoding.ASCII.GetBytes(xmAddressSectionTemplate);
            MemoryStream Addressstream = new MemoryStream(byteAddressArray);

            //byte[] byteSectionsArray = Encoding.ASCII.GetBytes(xmlEmptySectionsTemplate);
            //MemoryStream Sectionsstream = new MemoryStream(byteSectionsArray);


            byte[] byteHtmlArray = Encoding.UTF8.GetBytes(xmlHtmlSectionTemplate);
            MemoryStream HtmlStream = new MemoryStream(byteHtmlArray);

            byte[] byteReferenceArray = Encoding.ASCII.GetBytes(ReferenceSectionTemplate);
            MemoryStream ReferencesStream = new MemoryStream(byteReferenceArray);

            byte[] byteInvoiceSummaryArray = Encoding.ASCII.GetBytes(xmlInvoiceSummaryTemplate);
            MemoryStream InvoiceSummaryStream = new MemoryStream(byteInvoiceSummaryArray);

            byte[] byteSupplyDetailArray = Encoding.ASCII.GetBytes(xmlSupplyDetailSectionTemplate);
            MemoryStream SupplyDetailStream = new MemoryStream(byteSupplyDetailArray);

            byte[] byteContractualConditionsArray = Encoding.ASCII.GetBytes(xmlContractualConditionsSectionTemplate);
            MemoryStream ContractualConditionsStream = new MemoryStream(byteContractualConditionsArray);

            byte[] byteConsumptionArray = Encoding.ASCII.GetBytes(xmlConsumptionSectionTemplate);
            MemoryStream ConsumptionStream = new MemoryStream(byteConsumptionArray);

            byte[] byteExpiredDocumentArray = Encoding.ASCII.GetBytes(xmlExpiredDocumentSectionTemplate);
            MemoryStream ExpiredDocumentStream = new MemoryStream(byteExpiredDocumentArray);

            byte[] byteInvoiceArray = Encoding.ASCII.GetBytes(xmlInvoiceSectionTemplate);
            MemoryStream InvoiceStream = new MemoryStream(byteInvoiceArray);

            byte[] byteDocumentArray = Encoding.ASCII.GetBytes(xmlDocumentTemplate);
            MemoryStream DocumentStream = new MemoryStream(byteDocumentArray);

            byte[] byteSummaryArray = Encoding.ASCII.GetBytes(xmlSummaryDataSectionsTemplate);
            MemoryStream SummaryStream = new MemoryStream(byteSummaryArray);

            byte[] byteChartArray = Encoding.ASCII.GetBytes(xmlChartDataSectionsTemplate);
            MemoryStream ChartDataStream = new MemoryStream(byteChartArray);

            byte[] byteEmptyArray = Encoding.ASCII.GetBytes(xmlEmptySectionsTemplate);
            MemoryStream EmptySectionStream = new MemoryStream(byteEmptyArray);

            byte[] bytePrescrizioneArray = Encoding.ASCII.GetBytes(xmlPrescrizioneTemplate);
            MemoryStream PrescrizioneStream = new MemoryStream(bytePrescrizioneArray);

            //FileStream PostalSheetStream = new FileStream(PostalSheetPath, System.IO.FileMode.Open, System.IO.FileAccess.Read,
            //      System.IO.FileShare.Read);
            //FileStream PostalSheetStream = null;

            byte[] bytePostalFeedsArray = Encoding.ASCII.GetBytes(xmlPostalFeedsTemplate);
            MemoryStream PostalFeedsStream = new MemoryStream(bytePostalFeedsArray);

            var PostalSheetStream = gen.GetPostalDocument(PostalFeedsStream);


            byte[] bytePostalFeedsArrayR = Encoding.ASCII.GetBytes(xmlPostalFeedsTemplateR);
            MemoryStream PostalFeedsStreamR = new MemoryStream(bytePostalFeedsArrayR);
            int Rate = 3;
            
              var PostalSheetStreamR = gen.PagoPa(PostalFeedsStreamR,Rate);
            
            //var stream = gen.GeneratePrint(Headerstream, Addressstream, HtmlStream, ReferencesStream, InvoiceSummaryStream, SupplyDetailStream, ContractualConditionsStream, ConsumptionStream, ExpiredDocumentStream, InvoiceStream, DocumentStream, SummaryStream, ChartDataStream, EmptySectionStream, Footerstream,null, PrescrizioneStream,null, PostalSheetStream);
            //byte[] bytePostalFeedsArray = Encoding.ASCII.GetBytes(xmlPostalFeedsTemplate);
            //MemoryStream PostalFeedsStream = new MemoryStream(bytePostalFeedsArray);
            //var stream = gen.GetPostalDocument(PostalFeedsStream);

            if (PostalSheetStreamR != null)
            {
                using (var fileStream = File.Create(basePath + @"/InvoicePdf/result_Invoice_Rate_3_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_sstt") + ".pdf"))
                {
                    PostalSheetStreamR.Seek(0, SeekOrigin.Begin);
                    PostalSheetStreamR.CopyTo(fileStream);
                }
            }

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            PrintGenerator gen = new PrintGenerator();

            var appDomain = System.AppDomain.CurrentDomain;
            var basePath = appDomain.RelativeSearchPath ?? appDomain.BaseDirectory;
            var PostalFeedsXmlPathR = basePath + @"\HtmlTemplates\RateData.xml";
            string xmlPostalFeedsTemplateR = System.IO.File.ReadAllText(PostalFeedsXmlPathR);
            byte[] bytePostalFeedsArrayR = Encoding.ASCII.GetBytes(xmlPostalFeedsTemplateR);
            MemoryStream PostalFeedsStreamR = new MemoryStream(bytePostalFeedsArrayR);

            int Rate = 2;
            var PostalSheetStreamR = gen.PagoPa(PostalFeedsStreamR, Rate);

            

            if (PostalSheetStreamR != null)
            {
                using (var fileStream = File.Create(basePath + @"/InvoicePdf/result_Invoice_Rate_2_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_sstt") + ".pdf"))
                {
                    PostalSheetStreamR.Seek(0, SeekOrigin.Begin);
                    PostalSheetStreamR.CopyTo(fileStream);
                }
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            PrintGenerator gen = new PrintGenerator();

            var appDomain = System.AppDomain.CurrentDomain;
            var basePath = appDomain.RelativeSearchPath ?? appDomain.BaseDirectory;
            var PostalFeedsXmlPathR = basePath + @"\HtmlTemplates\Test.xml";
            string xmlPostalFeedsTemplateR = System.IO.File.ReadAllText(PostalFeedsXmlPathR);
            byte[] bytePostalFeedsArrayR = Encoding.ASCII.GetBytes(xmlPostalFeedsTemplateR);
            MemoryStream PostalFeedsStreamR = new MemoryStream(bytePostalFeedsArrayR);

            int Rate = 1;
            var PostalSheetStreamR = gen.PagoPa(PostalFeedsStreamR, Rate);



            if (PostalSheetStreamR != null)
            {
                using (var fileStream = File.Create(basePath + @"/InvoicePdf/result_Invoice_Rate_1_" + DateTime.Now.ToString("dd_MM_yyyy_HH_mm_sstt") + ".pdf"))
                {
                    PostalSheetStreamR.Seek(0, SeekOrigin.Begin);
                    PostalSheetStreamR.CopyTo(fileStream);
                }
            }

        }
    }
}
